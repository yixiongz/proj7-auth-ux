# Project 7: Adding authentication and user interface to brevet time calculator service

-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: A web based local ACP Brevet Control Times Calculator calculator with a database It receive the input data from frontend, send the data to backend by AJAX, after solve the data the result by be sent back by the same way. When click the submit button, data from input will be stored in to, when click the display button, the data which are stored in the database will be displayed on the browser. There is also a RESTful service to expose what is stored in MongoDB with several basic APIs. By entry specific url, it will display the pertinent content.

There is an Authenticating the services
    it has a register - login - logout system with token.
UI contains 
    (a) remember me, (b) logout, and (c) CSRF protection.
