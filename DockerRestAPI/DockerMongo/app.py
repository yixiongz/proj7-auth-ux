import os
import flask
import arrow
from flask import Flask, redirect, url_for, request, render_template, Response, flash
from flask_restful import Resource, Api
from pymongo import MongoClient
import acp_times
import config
import logging
from io import StringIO
import csv
from forms import LoginForm
from register_forms import RegisterForm

from itsdangerous import (TimedJSONWebSignatureSerializer \
                          as Serializer, BadSignature, \
                          SignatureExpired)
import time
from flask_httpauth import HTTPBasicAuth
from flask_login import (LoginManager, current_user, login_required,
                         login_user, logout_user, UserMixin,
                         confirm_login, fresh_login_required)


app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
client = MongoClient("dockerrestapi_db_1", 27017)
db = client.tododb
db_user = client.userdb

db_array = []
id = 0
auth = HTTPBasicAuth()

class User(UserMixin):
    def __init__(self, name, id, active=True):
        self.name = name
        self.id = id
        self.active = active
    
    def is_active(self):
        return self.active

USERS = {
}



app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'

app.config.from_object(__name__)

# step 1 in slides
login_manager = LoginManager()

# step 6 in the slides
login_manager.login_view = "login"
login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

# step 2 in slides
@login_manager.user_loader
def load_user(id):
    return USERS.get(int(id))

login_manager.setup_app(app)



def generate_auth_token(expiration, id):
    #s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
    s = Serializer('test1234@#$', expires_in=expiration)
    # pass index of user
    return s.dumps({'id': id})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    # valid token, but expired
    except BadSignature:
        return None    # invalid token
    return "Success"

#@app.route('app/token')
#@auth.login_required
#def get_token()
#    token = generate_auth_token(600)


@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    if items == []:
        return render_template('unsubmit.html', items = items)
    else:
        return render_template('todo.html', items = items)



@app.route('/new', methods=['POST'])
def new():
    global db_array
    size = len(db_array)
    for i in range(0, int(size), 4):
        item_doc = {
            'get_brevet_km': db_array[i],
            'arrow_time': db_array[i+1],
            'open_time': db_array[i+2],
            'close_time': db_array[i+3]
        }
        db.tododb.insert_one(item_doc)
    return redirect(url_for('index'))

    
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
    global id
    form = RegisterForm()
    if form.validate_on_submit():
        items = db_user.userdb.find()
        for item in items:
            if item["username"] == form.username.data:
                flash('This username have been used!')
                return redirect(url_for('register'))
        id += 1
        new_user = {
            'username' : form.username.data,
            'password' : form.password.data,
            'id' : id
        #'remember' : form.remember.data
        }
        db_user.userdb.insert_one(new_user)
        USERS.update({id: User(form.username.data, id)})
        flash('Register success!')
        return redirect(url_for('index'))
    return render_template('register.html',  title='Register', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        items = db_user.userdb.find()
        for item in items:
            if item["username"] == form.username.data:
                if item["password"] == form.password.data:
                    flash('Login success!')
                    generate_auth_token(60, item["id"])
                    flash(generate_auth_token(60, item["id"]))
                    return render_template('calc.html')
        #return redirect(url_for('index'))
        flash('Incorrect username or password! Try again!')
        return redirect(url_for('login'))
    return render_template('login.html',  title='Sign In', form=form)


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash(u"Reauthenticated.")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    return redirect(url_for("index"))




###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    get_brevet_km = request.args.get('brevet_km', 999, type= int)
    get_begin_date = request.args.get('begin_date', '', type= str)
    get_begin_time = request.args.get('begin_time', '', type= str)


    format_time = get_begin_date + 'T' + get_begin_time + ':00.000000-08:00'
    arrow_time = arrow.get(format_time)

    open_time = acp_times.open_time(km, get_brevet_km, arrow_time)
    close_time = acp_times.close_time(km, get_brevet_km, arrow_time)
    result = {"open": open_time, "close": close_time}
    
    global db_array
    db_array.append(get_brevet_km)
    db_array.append(arrow_time.isoformat())
    db_array.append(open_time)
    db_array.append(close_time)
    return flask.jsonify(result=result)


#############
class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
        'Yet another laptop!'
            ]
        }

api.add_resource(Laptop, '/')

class listAll(Resource):
    def get(self):
        items = db.tododb.find()
        opentime = []
        closetime = []
        for item in items:
            opentime.append(item["open_time"])
            closetime.append(item["close_time"])
        return {
            'Open_time': opentime,
            'Close_time': closetime
        }
api.add_resource(listAll, '/listAll', '/listAll/json')


class listOpenOnly(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        opentime = []
        result = []
        count = 0
        for item in items:
            opentime.append(item["open_time"])
            count += 1
        opentime.sort()
        if top != None:
            if top <= count:
                for i in range(top):
                    result.append(opentime[i])
                return {
                'Open_time': result
                } 
            else:
                return {
                'Open_time': opentime
                }
        else:   
            return {
            'Open_time': opentime
            }
api.add_resource(listOpenOnly, '/listOpenOnly', '/listOpenOnly/json')


class listCloseOnly(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        closetime = []
        result = []
        count = 0
        for item in items:
            closetime.append(item["close_time"])
            count += 1
        closetime.sort()
        if top != None:
            if top <= count:
                for i in range(top):
                    result.append(closetime[i])
                return {
                'Close_time': result
                }   
            else:
                return {
                'Close_time': closetime
                }
        else:   
            return {
            'Close_time': closetime
            }
        
api.add_resource(listCloseOnly, '/listCloseOnly', '/listCloseOnly/json')

class listAllcsv(Resource):
    def get(self):
        items = db.tododb.find()
        opentime = " "
        closetime = " "
        for item in items:
            opentime += str(item["open_time"]) + ", "
            closetime += str(item["close_time"]) + ", "
        return opentime + closetime

api.add_resource(listAllcsv, '/listAll/csv')


class listOpenOnlycsv(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        opentime = []
        result = []
        count = 0
        for item in items:
            opentime.append(item["open_time"])
            count += 1
        opentime.sort()
        opentime_str = " "
        if top != None:
            if top <= count:
                for i in range(top):
                    opentime_str += str(opentime[i]) + ', '
                return opentime_str
            else:   
                for item in opentime:
                    opentime_str += str(item) + ", "
                return opentime_str
        else:   
            for item in opentime:
                opentime_str += (item + ", ")
            return opentime_str

api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')

class listCloseOnlycsv(Resource):
    def get(self):
        items = db.tododb.find()
        top = request.args.get('top', type = int)
        closetime = []
        result = []
        count = 0
        for item in items:
            closetime.append(item["close_time"])
            count += 1
        closetime.sort()
        closetime_str = " "
        if top != None:
            if top <= count:
                for i in range(top):
                    closetime_str += str(closetime[i]) + ", "
                return closetime_str
            else:   
                for item in closetime:
                    closetime_str += str(item) + ", "
                return closetime_str
        else:   
            for item in closetime:
                closetime_str += str(item) + ", "
            return closetime_str

api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug = True)

